#!/bin/bash

# Keywords to watch for in raw device input
event_play='*play*'
event_next='*next*'
event_prev='*previous*'
event_up='*volume_up*'
event_down='*volume_down*'

# Truncate the dump file before we begin
touch /dev/shm/mpd-remote
truncate -s 0 /dev/shm/mpd-remote

# Watch the mpd-remote dump file
tail -n 1 -f /dev/shm/mpd-remote | while read line; do
echo $line
case $line in
  ($event_play) mpc toggle ;;
  ($event_next) mpc next ;;
  ($event_prev) mpc prev ;;
  ($event_up) mpc volume +5 ;;
  ($event_down) mpc volume -5 ;;
esac
done