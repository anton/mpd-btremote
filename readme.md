# MPD remote
This library allows you to add physical remote support to Mopidy.

There are two parts to this library:
- The listener scripts. These translate the raw input to commands that this script can interpret
- The remote script. This sends the actual commands that need to be processed to `mpc`.

## Setup Bluetooth devices

Bluetooth needs to be enabled on the device.

Use `bluetoothctl` to scan for the remote, then trust it and pair it.

If there are any connection issues later, remove the device and then trust/pair it again.

```
sudo bluetoothctl

```

Use these commands:
```
  list                       List available controllers
  show [ctrl]                Controller information
  select <ctrl>              Select default controller
  devices                    List available devices
  paired-devices             List paired devices
  power <on/off>             Set controller power
  pairable <on/off>          Set controller pairable mode
  discoverable <on/off>      Set controller discoverable mode
  agent <on/off/capability>  Enable/disable agent with given capability
  default-agent              Set agent as the default one
  scan <on/off>              Scan for devices
  info <dev>                 Device information
  pair <dev>                 Pair with device
  trust <dev>                Trust device
  untrust <dev>              Untrust device
  block <dev>                Block device
  unblock <dev>              Unblock device
  remove <dev>               Remove device
  connect <dev>              Connect device
  disconnect <dev>           Disconnect device
  version                    Display version
  quit                       Quit program
```

See `/dev/input/event*` for paired Bluetooth devices.

You can then run `btlistener.sh /dev/input/event0` to start the listener.

## Setup RF devices

This functionality is planned and will be added in the future.

## Cron jobs

All input from devices is written to ram. It is important that the input files are truncated every so often so this file does not grow too large.

```
0 * * * * truncate -s 0 /dev/shm/mpd-remote # Truncates the mpd-remote input cache file
```

## Supervisor conf example

The `btlistener*` programs are listeners for separate Bluetooth remotes.

The `remote` processes the listener events.

Finally, all the programs are tethered into a group (`mpd-remote`) so it can be managed easily with Supervisor.

```
[program:btlistener0]
process_name=%(program_name)s
command=/opt/mpd-remote/btlistener.sh /dev/input/event0
autostart=true
autorestart=true
stderr_logfile=/var/log/mpd-remote-btlistener.err.log
stdout_logfile=/var/log/mpd-remote-btlistener.out.log
numprocs=1

[program:btlistener1]
process_name=%(program_name)s
command=/opt/mpd-remote/btlistener.sh /dev/input/event1
autostart=true
autorestart=true
stderr_logfile=/var/log/mpd-remote-btlistener.err.log
stdout_logfile=/var/log/mpd-remote-btlistener.out.log
numprocs=1

[program:btlistener2]
process_name=%(program_name)s
command=/opt/mpd-remote/btlistener.sh /dev/input/event2
autostart=true
autorestart=true
stderr_logfile=/var/log/mpd-remote-btlistener.err.log
stdout_logfile=/var/log/mpd-remote-btlistener.out.log
numprocs=1

[program:remote]
process_name=%(program_name)s
command=/opt/mpd-remote/remote.sh
autostart=true
autorestart=true
stderr_logfile=/var/log/mpd-remote.err.log
stdout_logfile=/var/log/mpd-remote.out.log
numprocs=1

[group:mpd-remote]
programs=btlistener0,btlistener1,btlistener2,remote
```
