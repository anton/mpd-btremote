#!/bin/bash

watchbt() {

  # Check if the specified input device exists, if not sleep and wait
  while [ ! -r $1 ]
  do
    sleep 1
  done
  
  echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] device_connected"  >> /dev/shm/mpd-remote
  
  # Watch the raw device input for keywords
  evtest "$1" | while read line; do
    case $line in
	  ($event_play) echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] play" >> /dev/shm/mpd-remote ;;
	  ($event_next) echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] next" >> /dev/shm/mpd-remote ;;
	  ($event_prev) echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] previous" >> /dev/shm/mpd-remote ;;
	  ($event_up) echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] volume_up" >> /dev/shm/mpd-remote ;;
	  ($event_down) echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] volume_down" >> /dev/shm/mpd-remote ;;
    esac
  done
  
  # If we get to this point, the device has gone to sleep. Start the function again
  echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] device_disconnected" >> /dev/shm/mpd-remote
  watchbt "$1"
}

# Keywords to watch for in raw device input
event_play='*code 164 (KEY_PLAYPAUSE), value 1*'
event_next='*code 163 (KEY_NEXTSONG), value 1*'
event_prev='*code 165 (KEY_PREVIOUSSONG), value 1*'
event_up='*code 115 (KEY_VOLUMEUP), value 1*'
event_down='*code 114 (KEY_VOLUMEDOWN), value 1*'

# Dump all media commands here, this file is stored in ram
touch /dev/shm/mpd-remote

echo "[$(date '+%Y-%m-%d %H:%M:%S')][bt:$1] listener_started"  >> /dev/shm/mpd-remote

# Watch device for input
watchbt "$1"

